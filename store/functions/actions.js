const actions = {
  burgerClicked (state, payload) {
    state.commit('toggleMobileMenu', payload)
  },
  setPageLoading (state, payload) {
    state.commit('togglePageLoading', payload)
  }
}

export default actions
