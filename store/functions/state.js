const state = {
  socialMenu: [
    {title: 'Facebook', url: 'https://www.facebook.com/mooveFWD', icon: ['fab', 'facebook-f']},
    {title: 'Google+', url: 'http://moove-full-stack-web-development.business.site/', icon: ['fab', 'google-plus-g']},
    {title: 'LinkedIn', url: 'https://www.linkedin.com/in/whpotgieter/', icon: ['fab', 'linkedin-in']},
    {title: 'GitHub', url: 'https://github.com/williepot', icon: ['fab', 'github-alt']}
  ],
  navMenu: [
    {title: 'about', link: '/'},
    {title: 'services', link: '/services'},
    {title: 'portfolio', link: '/portfolio'},
    {title: 'pricing', link: '/pricing'},
    {title: 'contact', link: '/contact'},
  ],
  mobileMenu: false,
  pageLoading: true
}

export default state
