const mutations = {
  toggleMobileMenu (state, payload) {
    state.mobileMenu = payload
  },
  togglePageLoading (state, payload) {
    state.pageLoading = payload
  }
}

export default mutations
