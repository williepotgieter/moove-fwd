const getters = {
  getSocialMenu (state) {
    return state.socialMenu
  },
  getNavMenu (state) {
    return state.navMenu
  },
  getBurgerState (state) {
    return state.mobileMenu
  },
  getPageLoading (state) {
    return state.pageLoading
  }
}

export default getters
