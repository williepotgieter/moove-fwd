import Vuex from 'vuex'

import state from './functions/state'
import mutations from './functions/mutations'
import actions from './functions/actions'
import getters from './functions/getters'

const createStore = ()=> {
  return new Vuex.Store({
    state,
    mutations,
    actions,
    getters
  })
}

export default createStore
