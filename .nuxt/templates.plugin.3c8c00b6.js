import Vue from 'vue'
import fontawesome from '@fortawesome/fontawesome'
import FontAwesomeIcon from '@fortawesome/vue-fontawesome'
fontawesome.config = {
  autoAddCss: false,
}



  
    
      import  { faAngleUp as fortawesomefontawesomefreesolid_faAngleUp } from '@fortawesome/fontawesome-free-solid'
      fontawesome.library.add(fortawesomefontawesomefreesolid_faAngleUp)
    
      import  { faGlobe as fortawesomefontawesomefreesolid_faGlobe } from '@fortawesome/fontawesome-free-solid'
      fontawesome.library.add(fortawesomefontawesomefreesolid_faGlobe)
    
      import  { faLock as fortawesomefontawesomefreesolid_faLock } from '@fortawesome/fontawesome-free-solid'
      fontawesome.library.add(fortawesomefontawesomefreesolid_faLock)
    
      import  { faHome as fortawesomefontawesomefreesolid_faHome } from '@fortawesome/fontawesome-free-solid'
      fontawesome.library.add(fortawesomefontawesomefreesolid_faHome)
    
      import  { faServer as fortawesomefontawesomefreesolid_faServer } from '@fortawesome/fontawesome-free-solid'
      fontawesome.library.add(fortawesomefontawesomefreesolid_faServer)
    
      import  { faCloudUploadAlt as fortawesomefontawesomefreesolid_faCloudUploadAlt } from '@fortawesome/fontawesome-free-solid'
      fontawesome.library.add(fortawesomefontawesomefreesolid_faCloudUploadAlt)
    
      import  { faSearch as fortawesomefontawesomefreesolid_faSearch } from '@fortawesome/fontawesome-free-solid'
      fontawesome.library.add(fortawesomefontawesomefreesolid_faSearch)
    
      import  { faDatabase as fortawesomefontawesomefreesolid_faDatabase } from '@fortawesome/fontawesome-free-solid'
      fontawesome.library.add(fortawesomefontawesomefreesolid_faDatabase)
    
      import  { faDesktop as fortawesomefontawesomefreesolid_faDesktop } from '@fortawesome/fontawesome-free-solid'
      fontawesome.library.add(fortawesomefontawesomefreesolid_faDesktop)
    
      import  { faWrench as fortawesomefontawesomefreesolid_faWrench } from '@fortawesome/fontawesome-free-solid'
      fontawesome.library.add(fortawesomefontawesomefreesolid_faWrench)
    
      import  { faExchangeAlt as fortawesomefontawesomefreesolid_faExchangeAlt } from '@fortawesome/fontawesome-free-solid'
      fontawesome.library.add(fortawesomefontawesomefreesolid_faExchangeAlt)
    
      import  { faMapMarkerAlt as fortawesomefontawesomefreesolid_faMapMarkerAlt } from '@fortawesome/fontawesome-free-solid'
      fontawesome.library.add(fortawesomefontawesomefreesolid_faMapMarkerAlt)
    
      import  { faChevronCircleRight as fortawesomefontawesomefreesolid_faChevronCircleRight } from '@fortawesome/fontawesome-free-solid'
      fontawesome.library.add(fortawesomefontawesomefreesolid_faChevronCircleRight)
    
      import  { faCheck as fortawesomefontawesomefreesolid_faCheck } from '@fortawesome/fontawesome-free-solid'
      fontawesome.library.add(fortawesomefontawesomefreesolid_faCheck)
    
  


  
    
      import  { faFacebookF as node_modulesfortawesomefreebrandssvgicons_faFacebookF } from '@/node_modules/@fortawesome/free-brands-svg-icons'
      fontawesome.library.add(node_modulesfortawesomefreebrandssvgicons_faFacebookF)
    
      import  { faGooglePlusG as node_modulesfortawesomefreebrandssvgicons_faGooglePlusG } from '@/node_modules/@fortawesome/free-brands-svg-icons'
      fontawesome.library.add(node_modulesfortawesomefreebrandssvgicons_faGooglePlusG)
    
      import  { faLinkedinIn as node_modulesfortawesomefreebrandssvgicons_faLinkedinIn } from '@/node_modules/@fortawesome/free-brands-svg-icons'
      fontawesome.library.add(node_modulesfortawesomefreebrandssvgicons_faLinkedinIn)
    
      import  { faGithubAlt as node_modulesfortawesomefreebrandssvgicons_faGithubAlt } from '@/node_modules/@fortawesome/free-brands-svg-icons'
      fontawesome.library.add(node_modulesfortawesomefreebrandssvgicons_faGithubAlt)
    
      import  { faWhatsapp as node_modulesfortawesomefreebrandssvgicons_faWhatsapp } from '@/node_modules/@fortawesome/free-brands-svg-icons'
      fontawesome.library.add(node_modulesfortawesomefreebrandssvgicons_faWhatsapp)
    
  


  
    
      import  { faEnvelope as node_modulesfortawesomefreeregularsvgicons_faEnvelope } from '@/node_modules/@fortawesome/free-regular-svg-icons'
      fontawesome.library.add(node_modulesfortawesomefreeregularsvgicons_faEnvelope)
    
  


Vue.component('fa', FontAwesomeIcon)