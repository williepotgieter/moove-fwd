module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    htmlAttrs: {
      lang: 'en',
    },
    title: 'Moove Full-stack Web Development',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'mooveFWD Full-stack Web Development. Freelance web developer offering full-stack web development services.' },
      {
        hid: 'keywords',
        name: 'keywords',
        content: 'vue, nuxt, javascript, web-development, freelance, development, web-design, web-hosting, static website'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/brand/favicon.png' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: false,
  /*
  ** Load global CSS
  */
  css: [
    '~assets/css/spectre.min.css',
    '~node_modules/spectre.css/dist/spectre-exp.min.css',
    '~node_modules/animate.css/animate.min.css',
    '~assets/sass/main.scss'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Axios
    '@nuxtjs/axios',
    // Fontawesome
    ['nuxt-fontawesome', {
      component: 'fa',
      imports: [
        {
          set: '@fortawesome/fontawesome-free-solid',
          icons: [
            'faAngleUp',
            'faGlobe',
            'faLock',
            'faHome',
            'faServer',
            'faCloudUploadAlt',
            'faSearch',
            'faDatabase',
            'faDesktop',
            'faWrench',
            'faExchangeAlt',
            'faMapMarkerAlt',
            'faChevronCircleRight',
            'faCheck'
          ]
        },
        {
          set: '@/node_modules/@fortawesome/free-brands-svg-icons',
          icons: [
            'faFacebookF',
            'faGooglePlusG',
            'faLinkedinIn',
            'faGithubAlt',
            'faWhatsapp'
          ]
        },

        {
          set: '@/node_modules/@fortawesome/free-regular-svg-icons',
          icons: [
            'faEnvelope'
          ]
        }

        // **** USAGE ****
        //   example: fa(:icon="['fab', 'apple']")
        // ***************
      ]
    }],
    // Use SASS variables globally
    ['nuxt-sass-resources-loader', [
        '~assets/sass/variables/_colours.scss',
        '~assets/sass/variables/_components.scss',
        '~assets/sass/variables/_breakpoints.scss',
        '~assets/sass/utilities/_mixins.scss'
    ]],
    // Google Analytics config
    ['@nuxtjs/google-analytics', {
      id: 'UA-117161769-1'
    }]
  ],
  // Axios configuration
  axios: {

  },
  /*
  ** Nuxt.js plugins
  */
  plugins: [

  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {

      config.resolve.alias['@fortawesome/free-regular-svg-icons$'] = '@fortawesome/free-regular-svg-icons/shakable.es.js'
      config.resolve.alias['@fortawesome/free-brands-svg-icons$'] = '@fortawesome/free-brands-svg-icons/shakable.es.js'
      config.resolve.alias['@fortawesome/fontawesome-free-solid$'] = '@fortawesome/fontawesome-free-solid/shakable.es.js'
/*
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      */
    }
  },
  /*
  ** Environment variables
  */
  env : {
    // Whatsapp API
    whatsappApiUrl: "https://api.whatsapp.com/send?phone=",
    whatsappApiKey: "841642405293",
    //Contact form API details
    mooveAPIurl: "https://moove-mailer.herokuapp.com/api/sendmail/",
    mooveAPIkey: "5ad9d62b7f79a254a1539dd4"
  }
}
