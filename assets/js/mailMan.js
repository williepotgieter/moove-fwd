const base64 = require('base-64')
const utf8 = require('utf8')
import axios from 'axios'

export function sendMail (data, key) {

  const messageData = {
    id: key,
    contact: {
      name: data.name.value,
      email: data.email.value,
      subject: data.subject.value,
      message: data.message.value
    }
  }

  const encryptedData = base64.encode(utf8.encode(JSON.stringify(messageData)))

  var config = {
      headers: {
          'Content-Type': 'text/plain'
      },
     responseType: 'text'
  }

  axios.post(process.env.mooveAPIurl, encryptedData, config)
}
